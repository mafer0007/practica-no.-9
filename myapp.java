package cuenta;

import java.time.LocalDate;
import java.util.ArrayList;

import clases.cuenta;

public class myapp {
	static void run() {

		double totalcuenta;
		// creamos la cuenta
		cuenta cuenta1;
		cuenta1 = new cuenta(17, "Marifer", LocalDate.of(2021, 02, 17), 1200.d, LocalDate.of(2021, 02, 17));

		// consultamos saldo
		totalcuenta = cuenta1.getSaldo();
		System.out.println("total actual en la cuenta:" + totalcuenta);

		// hacemos un retiro
		double retirar = 300;
		System.out.println("se retira en la cuenta:" + retirar);
		cuenta1.retirar(retirar);

		// consultamos saldo
		totalcuenta = cuenta1.getSaldo();
		System.out.println("total actual en la cuenta:" + totalcuenta);

		// hacemos un deposito
		double deposito = 500;
		System.out.println("se deposita en la cuenta:" + deposito);
		cuenta1.deposito(deposito);

		// consultamos saldo
		totalcuenta = cuenta1.getSaldo();
		System.out.println("total actual en la cuenta:" + totalcuenta);

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		run();
	}

}
