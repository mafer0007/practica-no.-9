package clases;

import java.time.LocalDate;

public class cuenta implements Comparable<cuenta> {

	private int numeroCuenata;
	private String nombreCliente;
	private LocalDate fechaApertura;
	private double saldo;
	private LocalDate fechaUltimaActualizacion;

	public cuenta(int numeroCuenta, String nombreCliente, LocalDate fechaApertura, double saldo,
			LocalDate fechaUltimaActualizacion) {
		this.numeroCuenata = numeroCuenta;
		this.nombreCliente = nombreCliente;
		this.fechaApertura = fechaApertura;
		this.saldo = saldo;
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;

	}

	public int getNumeroCuenata() {
		return numeroCuenata;
	}

	public void setNumeroCuenata(int numeroCuenata) {
		this.numeroCuenata = numeroCuenata;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public LocalDate getFechaApertura() {
		return fechaApertura;
	}

	public void setFechaApertura(LocalDate fechaApertura) {
		this.fechaApertura = fechaApertura;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public LocalDate getFechaUltimaActualizacion() {
		return fechaUltimaActualizacion;
	}

	public void setFechaUltimaActualizacion(LocalDate fechaUltimaActualizacion) {
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;
	}

	// metodo deposito
	public void deposito(double cantidad) {
		saldo = saldo + cantidad;
	}

	// metodo retiro
	public void retirar(double cantidad) {
		saldo = saldo - cantidad;
		// Fin del mtodo retirar

	}

	@Override
	public String toString() {
		return "cuenta [numeroCuenata=" + numeroCuenata + ", nombreCliente=" + nombreCliente + ", fechaApertura="
				+ fechaApertura + ", saldo=" + saldo + ", fechaUltimaActualizacion=" + fechaUltimaActualizacion + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fechaApertura == null) ? 0 : fechaApertura.hashCode());
		result = prime * result + ((fechaUltimaActualizacion == null) ? 0 : fechaUltimaActualizacion.hashCode());
		result = prime * result + ((nombreCliente == null) ? 0 : nombreCliente.hashCode());
		result = prime * result + numeroCuenata;
		long temp;
		temp = Double.doubleToLongBits(saldo);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		cuenta other = (cuenta) obj;
		if (fechaApertura == null) {
			if (other.fechaApertura != null)
				return false;
		} else if (!fechaApertura.equals(other.fechaApertura))
			return false;
		if (fechaUltimaActualizacion == null) {
			if (other.fechaUltimaActualizacion != null)
				return false;
		} else if (!fechaUltimaActualizacion.equals(other.fechaUltimaActualizacion))
			return false;
		if (nombreCliente == null) {
			if (other.nombreCliente != null)
				return false;
		} else if (!nombreCliente.equals(other.nombreCliente))
			return false;
		if (numeroCuenata != other.numeroCuenata)
			return false;
		if (Double.doubleToLongBits(saldo) != Double.doubleToLongBits(other.saldo))
			return false;
		return true;
	}

	@Override
	public int compareTo(cuenta o) {
		// TODO Auto-generated method stub
		if (this.numeroCuenata != o.getNumeroCuenata())
			;
		else if (this.nombreCliente.compareTo(o.getNombreCliente()) != 0)
			return this.nombreCliente.compareTo(o.getNombreCliente());
		else if (this.fechaApertura.compareTo(o.fechaApertura) != 0)
			return this.fechaApertura.compareTo(o.getFechaApertura());
		else if (this.fechaUltimaActualizacion.compareTo(o.getFechaUltimaActualizacion()) != 0)
			return this.fechaUltimaActualizacion.compareTo(o.getFechaUltimaActualizacion());
		return 0;
	}

}
